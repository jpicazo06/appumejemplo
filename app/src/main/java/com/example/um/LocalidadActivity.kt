package com.example.um

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import com.example.um.Modelos.Preferencias.Preferencias
import com.example.um.Modelos.Preferencias.Preferencias.set
import com.example.um.Modelos.Preferencias.Preferencias.get
import kotlinx.android.synthetic.main.activity_localidad.*
import kotlinx.android.synthetic.main.dashboard.*
import kotlinx.android.synthetic.main.dashboard.toolbar
import kotlinx.android.synthetic.main.header_nav.*
import kotlinx.android.synthetic.main.header_nav.view.*
import kotlinx.android.synthetic.main.localidades.*

class LocalidadActivity : AppCompatActivity() {


    private lateinit var toggle: ActionBarDrawerToggle

    private val preferencias by lazy {
        Preferencias.defaultPrefs(this)
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_localidad)





        setSupportActionBar(toolbar)
        getPrefencias()

        toggle = ActionBarDrawerToggle(
            this,
            drawer_layoutDos,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_open
        )

        drawer_layoutDos.addDrawerListener(toggle)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)


        val id = getIntent().getExtras()?.getString("localidad")

        this.toast(id.toString())


        val prefs = Preferencias.defaultPrefs(
            this@LocalidadActivity
        )

        val token =   prefs["jwt",""]
        val nombre = prefs["nombre",""]
        val apellido = prefs["apellido",""]
        val empresa = prefs["empresa",""]



        val nombretv = TextView(this@LocalidadActivity)
        nombretv.text = empresa



      nombreGeneral.text = "empresa"
        contenedorPRueba.addView(nombretv)
        pruebaToquen.text = token



    }

    private fun getPrefencias() {







    }
}
