package com.example.um.Dashboard

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.GridLayoutManager
import com.example.um.Modelos.Adaptadores.AdaptadorRelacionEmpresa
import com.example.um.Modelos.Preferencias.Preferencias
import com.example.um.Modelos.Preferencias.Preferencias.get
import com.example.um.Modelos.Preferencias.Preferencias.set
import com.example.um.Modelos.UsuarioConectado
import com.example.um.Modelos.UsuarioPerfil
import com.example.um.R
import com.example.um.services.Servicios
import com.example.um.toast
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.dashboard.*
import kotlinx.android.synthetic.main.header_nav.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardActivity : AppCompatActivity() {

//    private lateinit var drawer: DrawerLayout

    private val servicio by lazy {
        Servicios.create()
    }


    private val preferencias by lazy {
        Preferencias.defaultPrefs(this)

    }

    private lateinit var toggle: ActionBarDrawerToggle

    private val arrayEmpresas = ArrayList<UsuarioPerfil>()
    private val fragmento = supportFragmentManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)


        val token = preferencias["jwt", ""]

        Log.d("tokenDos", token)




        datosUsuario(token)
        usuarioPerfil(token, this)


        setSupportActionBar(toolbar)

        toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_open
        )

        drawer_layout.addDrawerListener(toggle)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)


    }

    private fun usuarioPerfil(token: String, contexto: Context) {

        val call = servicio.getUsuarioPErfil("Bearer $token")


        call.enqueue(object : Callback<UsuarioPerfil> {
            override fun onFailure(call: Call<UsuarioPerfil>, t: Throwable) {
                this@DashboardActivity.toast(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<UsuarioPerfil>,
                response: Response<UsuarioPerfil>
            ) {


                if (response.isSuccessful) {
                    if (response != null) {
                        val resDos = response.body()




                        if (resDos != null) {
                            arrayEmpresas.add(resDos)




                            Log.d("arrayDos", resDos?.relaciones[0].empresa.nombre.toString())



                            rvEmpresas.layoutManager = GridLayoutManager(this@DashboardActivity, 1)
                            rvEmpresas.adapter = AdaptadorRelacionEmpresa(arrayEmpresas, this@DashboardActivity)


                        }
//
                    }
                }


            }

        })


    }

    private fun datosUsuario(token: String) {
        val Call = servicio.getUsuario("Bearer $token")

        Call.enqueue(
            object : Callback<UsuarioConectado> {
                override fun onFailure(call: Call<UsuarioConectado>, t: Throwable) {
                    this@DashboardActivity.toast(t.localizedMessage)
                }

                override fun onResponse(
                    call: Call<UsuarioConectado>,
                    response: Response<UsuarioConectado>
                ) {
                    if (response.isSuccessful) {
                        val res = response.body()

                        if (res != null) {



                            val nombre = Preferencias.defaultPrefs(
                                this@DashboardActivity
                            )

                            nombre["nombre"] = res.relacion.persona.primer_nombre
                            nombre["apellido"] = res.relacion.persona.paterno
                            nombre["empresa"] = res.relacion.empresa.nombre

                            nombreGeneral.text = res.relacion.persona.primer_nombre
                            tvApellidoPaterno.text = res.relacion.persona.paterno

                            tvEmpresa.text = res.relacion.empresa.nombre


                        }
                    }
                }

            }
        )

    }


}
