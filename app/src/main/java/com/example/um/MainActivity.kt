package com.example.um

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.um.Dashboard.DashboardActivity
import com.example.um.Modelos.Preferencias.Preferencias
import com.example.um.Modelos.Preferencias.Preferencias.get
import com.example.um.Modelos.Preferencias.Preferencias.set
import com.example.um.Modelos.Token
import com.example.um.services.Servicios
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private val servicio:Servicios by lazy {
        Servicios.create()
    }

    private val laya by lazy {
        Snackbar.make(llLogin,"No se pueden dejar campos vacios",Snackbar.LENGTH_SHORT)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)






        btnIngresar.setOnClickListener{

            ingresarDashboard()



            
        }



    }





    private fun ingresarDashboard() {


        val preference = Preferencias.defaultPrefs(
            this
        )







        val psw = contrasenaDos.text.toString()
        val correoDos = correo.text.toString()

        if (psw != "" && correoDos != ""){
            val call = servicio.setUser(
                correoDos,
                psw)

            call.enqueue(object: Callback<Token>{
                override fun onFailure(call: retrofit2.Call<Token>, t: Throwable) {


                    this@MainActivity.toast(t.localizedMessage)

                }

                override fun onResponse(call: retrofit2.Call<Token>, response: Response<Token>) {

                    if (response.isSuccessful){

                        if (preference["jwt",""].contains(".")){
                            irMenu()
                        }else{

                        }
                        var res = response.body()

                        if (res==null){
                            this@MainActivity.toast("erro en el servidor")
                            return
                        }else{
                            this@MainActivity.toast("todo salio bien")
                            Log.d("token", res.token)

                            setPreferences(res.token)
                        }




                    }
                }

            })

        }else{
            laya.show()
        }
//            this.toast(correoDos)

    }

    private fun irMenu() {
        
        
             val irMenu = Intent(this, DashboardActivity::class.java)
                        
                         startActivity(irMenu)
        
        
    }

    private fun setPreferences(token:String) {


        val preferencia = Preferencias.defaultPrefs(
            this
        )

        preferencia["jwt"]= token





    }
}
