package com.example.um.Modelos.Adaptadores

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.um.LocalidadActivity
import com.example.um.Modelos.UsuarioPerfil
import com.example.um.R
import com.example.um.toast
import kotlinx.android.synthetic.main.item_empresas.view.*

// aqui agregamos una variable con un array list
class AdaptadorRelacionEmpresa(
    private val usuarioPerfil: ArrayList<UsuarioPerfil>,
    private val contexto: Context
) : RecyclerView.Adapter<AdaptadorRelacionEmpresa.ViewHolder>() {




    // crear la vista
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(
                parent.context
            )
                .inflate(
                    R.layout.item_empresas,
                    parent,
                    false
                )
        )


    }


    override fun getItemCount(): Int {

        return usuarioPerfil.size


    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.tvEmpresa.text =
            usuarioPerfil.get(0)?.relaciones[position]?.empresa?.nombre.toString()

        val idEmpresa = usuarioPerfil[0].relaciones[position].empresa.id.toString()

        holder.itemView.setOnClickListener(View.OnClickListener {
            contexto.toast(idEmpresa)



        })

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val tvEmpresa = itemView.tvEmpresa


    }

}