package com.example.um.Modelos

data class UsuarioPerfil(
    val relaciones: List<Relacione>
)

data class Relacione(
    val empresa: Empresa,
    val estatus: Int,
    val id: Int,
    val perfil: Perfil,
    val persona: Persona
)





