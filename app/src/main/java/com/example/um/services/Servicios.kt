package com.example.um.services

import com.example.um.Modelos.Token
import com.example.um.Modelos.UsuarioConectado
import com.example.um.Modelos.UsuarioPerfil
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface Servicios {

            @POST("login2/token")
                fun setUser(@Query("correo")correo:String,@Query("contrasena")contrasena:String):
                    Call<Token>

            @GET("usuarioconectado")
                fun getUsuario(@Header("Authorization") autHeader:String):
                        Call<UsuarioConectado>


            @GET("usuarioperfil")
                fun getUsuarioPErfil(@Header("Authorization")autHeader: String):
                        Call<UsuarioPerfil>

                companion object factory{

                    private const val BASE_URL = "http://129.144.51.42/desarrollos/admUM/public/"

                    fun create(): Servicios{
                        val retrofit = Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()

                        return retrofit.create(Servicios::class.java)
                    }
                }

                // en la actividad mandar llamar :
                //   private val Servicios: Servicios by lazy {
                 //   Servicios.create()
                //}
}